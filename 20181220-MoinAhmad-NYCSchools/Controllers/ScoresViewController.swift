//
//  ScoresViewController.swift
//  20181220-MoinAhmad-NYCSchools
//
//  Created by Moinuddin Ahmad on 12/20/18.
//  Copyright © 2018 MoinAhmad.com. All rights reserved.
//

import UIKit

class ScoresViewController: UIViewController {


    // MARK: - Variable & Outlet Initializations

    @IBOutlet var schoolNameLabel: UILabel!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var numOfTestTakers: UILabel!
    @IBOutlet var criticalReadingAvgScore: UILabel!
    @IBOutlet var mathAvgScore: UILabel!
    @IBOutlet var writingAvgScore: UILabel!
    
    var selectedRowInScoresVC:Int?
    
    var schools : [School]?
    
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "SAT Scores"
        
        if selectedRowInScoresVC != nil {
            
            // School name formatting and label output below.
            schoolNameLabel.lineBreakMode = .byWordWrapping
            schoolNameLabel.numberOfLines = 0
            schoolNameLabel.adjustsFontSizeToFitWidth = true
            schoolNameLabel.text = schools![selectedRowInScoresVC!].name
            
            // School's SAT test results label output below.
            if schools![selectedRowInScoresVC!].numOfTesters == "s" {

                errorLabel.isHidden = false
                errorLabel.text = "INCOMPLETE DATA"
                errorLabel.textAlignment = .center
                errorLabel.textColor = UIColor.red
                errorLabel.font = UIFont.boldSystemFont(ofSize: 29.0)
                
                numOfTestTakers.text = "--"
                criticalReadingAvgScore.text = "--"
                mathAvgScore.text = "--"
                writingAvgScore.text = "--"
                
            } else {

                errorLabel.isHidden = true

                numOfTestTakers.text = schools![selectedRowInScoresVC!].numOfTesters
                criticalReadingAvgScore.text = schools![selectedRowInScoresVC!].criticalReadingAvgScore
                mathAvgScore.text = schools![selectedRowInScoresVC!].mathAvgScore
                writingAvgScore.text = schools![selectedRowInScoresVC!].writingAvgScore

            }
        // In case there's any sort of loading/access issue, below is an error alert to notify the user.
        } else {
            
            // Alerting the user to a problem with loading the selected school's SAT scores.
            let alert = UIAlertController(title: "Error", message: "There's a problem accessing the SAT scores of this school. Please try again or restart the app.\n\nThank you.", preferredStyle: .alert)
            let closeAction = UIAlertAction(title: "Close", style: .default) { (action) in
                print("SYSLOG:  Alert closed by user.")
            }
            
            alert.addAction(closeAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }

}
