//
//  SchoolsViewController.swift
//  20181220-MoinAhmad-NYCSchools
//
//  Created by Moinuddin Ahmad on 12/20/18.
//  Copyright © 2018 MoinAhmad.com. All rights reserved.
//

import UIKit
import Foundation

// MARK: - Global Struct Initialization
struct School : Decodable {
    let name : String
    let numOfTesters : String
    let criticalReadingAvgScore : String
    let mathAvgScore : String
    let writingAvgScore : String
    
    enum CodingKeys: String, CodingKey {
        case name = "school_name"
        case numOfTesters = "num_of_sat_test_takers"
        case criticalReadingAvgScore = "sat_critical_reading_avg_score"
        case mathAvgScore = "sat_math_avg_score"
        case writingAvgScore = "sat_writing_avg_score"
    }
}

class SchoolsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {


    // MARK: - Variable & Outlet Initializations
    
    var Schools = [School]()

    @IBOutlet var schoolTableView: UITableView!

    
    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        schoolTableView.delegate = self
        schoolTableView.dataSource = self
        
        let urlString = "https://data.cityofnewyork.us/resource/734v-jeq5.json"
        guard let urlObj = URL(string: urlString) else { return }

        URLSession.shared.dataTask(with: urlObj) { (data, response, error) in
            print("SYSLOG:  In URLSession completion block")
            
            guard let data = data else { return }

            do {
                let schools = try JSONDecoder().decode([School].self, from: data)

                DispatchQueue.main.async {
                    for school in schools {
                        self.Schools.append(school)
                        self.schoolTableView.reloadData()
                    }
                }
 
            } catch let jsonError {
                print("ERROR: There's a problem getting decoding the JSON: \(jsonError)")
            }
        }.resume()

    }

    override func viewWillAppear(_ animated: Bool) {
        schoolTableView.reloadData()
    }
    
    
    // MARK: - Actions
    
    @IBAction func refresh(_ sender: UIBarButtonItem) {
        schoolTableView.reloadData()
    }


    // MARK: - Table View Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "schoolCell", for: indexPath)
        let school = Schools[indexPath.row]

        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = school.name

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("SYSLOG:  Row: \(indexPath.row)")
        
        let scoresVC = self.storyboard?.instantiateViewController(withIdentifier: "scoresVC") as! ScoresViewController
        scoresVC.schools = Schools
        
        scoresVC.selectedRowInScoresVC = indexPath.row
        
        self.navigationController?.pushViewController(scoresVC, animated: true)
    }

}
